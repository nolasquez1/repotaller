function getClientes(){
  var url = "https://services.odata.org/V4/Northwind/Northwind.svc/Customers"
  console.log(url);
  var request = new XMLHttpRequest();
  request.onreadystatechange = function() {
    if(this.readyState  == 4 || this.status ==200){
      //console.table(JSON.parse(request.responseText).value);
      clientesObtenidos = request.responseText;
      procesarClientes();
    }
  }
  request.open("GET", url, true);
  request.send();
}



function procesarClientes(){
  var JSONClientes = JSON.parse(clientesObtenidos);
  console.log(JSONClientes);
  var divTabla = document.getElementById("divTabla2");
  var tabla = document.createElement("table");
  var tbody = document.createElement("tbody");
  tabla.classList.add("table");
  tabla.classList.add("table-hover");

  var rutaBandera = "https://www.countries-ofthe-world.com/flags-normal/flag-of-";

  for(var i=0; i<JSONClientes.value.length; i++)
  {

    //console.log(JSONClientes.value[i].ProductName);
    var nuevaFila = document.createElement("tr");

    var columnaNombre = document.createElement("td");
    columnaNombre.innerText = JSONClientes.value[i].ContactName

    var columnaCiudad = document.createElement("td");
    columnaCiudad.innerText = JSONClientes.value[i].City

    var columnaPais = document.createElement("td");
    var imgBandera = document.createElement("img");
    pais = JSONClientes.value[i].Country;
    if (pais == "UK")
      pais = "United-Kingdom"
    imgBandera.src = rutaBandera+pais+".png";
    //imgBandera.class List.add("flag");

    columnaPais.appendChild(imgBandera);

    nuevaFila.appendChild(columnaNombre);
    nuevaFila.appendChild(columnaCiudad);
    nuevaFila.appendChild(columnaPais);

    tbody.appendChild(nuevaFila);
  }
  tabla.appendChild(tbody);
  divTabla.appendChild(tabla);
}
